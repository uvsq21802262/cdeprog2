package fr.uvsq;


public class Balance {
	
	private int number;
	
	/**
	 * Creates a balance
	 * @param number : amount of the money
	 * @throws IllegalAmountException if amount is negative
	 */
	public Balance(int number) throws IllegalAmountException {
		if (number <= 0) throw new IllegalAmountException();
		this.number = number;
	}

	
	/**
	 * Adds money to the account
	 * @param input : the amount of the money
	 * @throws IllegalAmountException if amount is negative
	 */
	public void addMoney(int input) throws IllegalAmountException {
		if (input < 0) throw new IllegalAmountException();
		number += input;
	}
	
	
	/**
	 * Subtracts money from the account
	 * @param input : the amount of money
	 * @throws IllegalAmountException if amount is negative
	 * @throws OverdraftException if money required is more than the possessed
	 */
	public void subtractMoney(int input) throws IllegalAmountException, OverdraftException {
		if (input < 0) throw new IllegalAmountException();
		int res = number - input;
		if (res < 0) throw new OverdraftException();
		number = res;
	}
	
	
	
	/**
	 * @return the quantity of the balance
	 */
	public int getNumber() {
		return number;
	}
}
