package main;

/**
 * Initiates the execution of the calculator program
 */
public enum CalculatorRPN {
	calculator;
	
	private void run() throws ArithmeticException {
		 InputRPN.startTerminal();
	}
	
	public static void main(String[] args) {
		try {
			calculator.run();
		} catch (ArithmeticException e) {
			System.out.println("Division by zero.\nFin du programme.");
		}
	}
	
	
}
