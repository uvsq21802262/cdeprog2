package anl;

import java.util.List;

import anl.item.Item;

public class SoldierAi extends CreatureAi {
	private StuffFactory factory;
	private int spreadcount;
	private List<String> messages;
	private FieldOfView fov;
	
	public SoldierAi(Creature creature, List<String> messages, FieldOfView fov, StuffFactory factory) {
		super(creature);
		this.messages = messages;
		this.fov = fov;
		this.factory = factory;
	}

	
	public void onUpdate(){
		if (spreadcount < 10 && Math.random() < 0.02)
			flock();
		if(attackStatus&&player!=null&&player.hp()>0) {
			
			if (creature.canSee(player.x, player.y, player.z)&&hunt(player)) {
//				if (canRangedWeaponAttack(player))
//					creature.rangedWeaponAttack(player);// that is way too dangerous
////				else if (canThrowAt(player))
////					creature.throwItem(getWeaponToThrow(), player.x, player.y, player.z);
//				else if(canAttack(player))
//					creature.meleeAttack(player);
//				else {
//				
//				}
			}else {
				wander();
			}
////			else if (canPickup())
////				creature.pickup();
		}
		else wander();
	}

	private boolean canAttack(Creature player) {
		if(this.creature.getNeighbour8().contains(player)) return true;
		else return false;
	}


	private void flock(){
		int x = creature.x + (int)(Math.random() * 20) - 10;
		int y = creature.y + (int)(Math.random() * 19) - 10;
		
		if (!creature.canEnter(x, y, creature.z))
			return;
		
		Creature child = factory.newSoldier(creature.z, messages, fov);
		child.x = x;
		child.y = y;
		child.z = creature.z;
		spreadcount++;
	}
	
	

	public void onEnter(int x, int y, int z, Tile tile){
		if (tile.isGround()){
			creature.x = x;
			creature.y = y;
			creature.z = z;
			
			Item item = creature.item(creature.x, creature.y, creature.z);
			if (item != null)
				creature.notify("There's a " + creature.nameOf(item) + " here.");
			
		} else if (tile.isDiggable()) {
			creature.dig(x, y, z);
		}
	}
	
	public void onNotify(String message){
		messages.add(message);
	}
	
	public boolean canSee(int wx, int wy, int wz) {
		return fov.isVisible(wx, wy, wz);
	}
	
	public void onGainLevel(){
	}

	public Tile rememberedTile(int wx, int wy, int wz) {
		return fov.tile(wx, wy, wz);
	}
}
