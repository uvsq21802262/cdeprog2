package anl.outil;

public class Constants {
	public static final int WORLD_WEIGHT = 90;
	public static final int WORLD_HEIGHT = 32;
	public static final int WORLD_DEPTH = 5;
	public final static int NUM_SOLDIER = 50;

	public static final int VISION_RADIUS = 9;
	public final static char PLAYER = '@';
	public final static char SOLDIER = 's';
	public static final char ROCK = ',';
	public static final char FOOD = '%';
	public static final char WEAPON = ')';
	public static final char ARMOR = '[';
	
	public final static String TALK_WITH_SOLDIER = new String("I talked a lot with a soldier.");// Asciipanel write string less than 80 He seems kind but has nothing to share, the war has made everyone more desperate");
	public static final String DONATION_FROM_SOLDIER = new String("I received some food from the soldier.");
	public static final String BACKPACK_FULL_DROP = new String("I cann't carry more things.");
	public static final String UNDER_ATTACK = new String("He attacked me.");
	public static final String GONE = new String("He left.");
	public static final String ATTACK = new String("I should never go there.");
	public static final String CHASED = new String("I'm running out of time. He chases up.");
	public static final String ESCAPED = new String("That was close. Luckily I get out of it.");
	public static final int DONATION_NUM = 5;
	
	public static final int ROCK_ATTACK_VALUE = 5;
	public static final int DAGGER_ATTACK_VALUE = 5;
	public static final int DAGGER_THROWN_ATTACK_VALUE = 5;
	public static final int STICK_ATTACK_VALUE = 20;
	public static final int STICK_THROWN_ATTACK_VALUE = 10;
	public static final int GUN_ATTACK_VALUE = 50;
	public static final int GUN_RANGE_ATTACK_VALUE = 85;
//	public static final int ROCK_ATTACK_VALUE = 5;
//	public static final int ROCK_ATTACK_VALUE = 5;
	public static final int MAX_FOOD = 1000;
	public static final int BREAD_VALUE = 300;
	public static final int APPLE_VALUE = 50;
	public static final int CAN_VALUE = 650;
	public static final int ARMOR_DEFENSE_VALUE = 60;
	public static final int BACKPACK_VOLUME = 9;
	
	
	
	
	
	
	
	
	
	
	
	

}
