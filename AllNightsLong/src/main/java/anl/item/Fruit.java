package anl.item;

import java.awt.Color;

import anl.outil.Constants;
import asciiPanel.AsciiPanel;

public class Fruit extends Food {
	public Fruit() {
		super(Constants.FOOD, AsciiPanel.brightRed, "apple", null, Constants.APPLE_VALUE);
	}

}
