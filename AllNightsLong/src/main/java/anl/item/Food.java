package anl.item;

import java.awt.Color;

public class Food extends Item {
	private int foodValue;
	public int foodValue() { return foodValue; }
	public void modifyFoodValue(int amount) { foodValue += amount; }

	public Food(char glyph, Color color, String name, String appearance, int foodValue){
		super(glyph, color, name, appearance);
		this.foodValue = foodValue;
	}
	
	public String details() {
		String details = "";

		if (foodValue != 0)
			details += "  food:" + foodValue;
		
		return details;
	}
}
