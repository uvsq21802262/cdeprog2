package anl.item;

import java.awt.Color;

import anl.outil.Constants;
import asciiPanel.AsciiPanel;

public class Can extends Food {

	public Can() {
		super(Constants.FOOD, AsciiPanel.brightBlack, "can", null, Constants.CAN_VALUE);
	}

}
