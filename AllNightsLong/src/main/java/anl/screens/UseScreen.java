package anl.screens;

import java.awt.event.KeyEvent;

import anl.Creature;
import anl.item.Armor;
import anl.item.Food;
import anl.item.Item;
import anl.item.Weapon;

public class UseScreen extends InventoryBasedScreen implements Screen {

	private Item item;
	public UseScreen(Creature player, Item item) {
		super(player);
		this.item = item;
	}

	@Override
	protected boolean isAcceptable(Item item) {
		return true;
	}

	@Override
	protected Screen use(Item item) {
		if(item instanceof Food) {
			player.eat((Food) item);
		}else if(item instanceof Weapon) {
			player.equip((Weapon) item);
		}else if(item instanceof Armor) {
			player.equip((Armor) item);
		}
		return null;
	}
	
	@Override
	public Screen respondToUserInput(KeyEvent key) {
	if(key.getKeyCode()==KeyEvent.VK_Y) {
			return use(item); 
		}else {	
			return null;
		}
	}

}
