package anl.screens;

import anl.Creature;
import anl.item.Armor;
import anl.item.Item;
import anl.item.Weapon;

public class EquipScreen extends InventoryBasedScreen {

	public EquipScreen(Creature player) {
		super(player);
	}

	protected String getVerb() {
		return "wear or wield";
	}

	protected boolean isAcceptable(Item item) {
		return ((Weapon)item).attackValue() > 0 || ((Armor)item).defenseValue() > 0;
	}

	protected Screen use(Item item) {
		player.equip((Weapon)item);
		return null;
	}
}
