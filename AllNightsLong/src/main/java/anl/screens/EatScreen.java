package anl.screens;

import anl.Creature;
import anl.item.Food;
import anl.item.Item;

public class EatScreen extends InventoryBasedScreen {

	public EatScreen(Creature player) {
		super(player);
	}

	@Override
	protected String getVerb() {
		return "eat";
	}

	@Override
	protected boolean isAcceptable(Item item) {
		return ((Food)item).foodValue() != 0;
	}

	@Override
	protected Screen use(Item item) {
		player.eat((Food)item);
		return null;
	}
}
