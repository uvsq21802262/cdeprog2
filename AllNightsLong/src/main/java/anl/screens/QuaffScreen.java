package anl.screens;

import anl.Creature;
import anl.item.Item;
import anl.item.Weapon;

public class QuaffScreen extends InventoryBasedScreen {

	public QuaffScreen(Creature player) {
		super(player);
	}

//	@Override
//	protected String getVerb() {
//		return "quaff";
//	}

	@Override//?
	protected boolean isAcceptable(Item item) {
		return ((Weapon)item).quaffEffect() != null;
	}

	@Override
	protected Screen use(Item item) {
		player.quaff(item);
		return null;
	}

}
